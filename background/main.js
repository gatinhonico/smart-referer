/**
 * Polyfill for `Promise.finally`
 */
Promise.prototype['finally'] = function (f) {
  return this.then(function (value) {
    return Promise.resolve(f()).then(function () {
      return value;
    });
  }, function (err) {
    return Promise.resolve(f()).then(function () {
      throw err;
    });
  });
}


/**
 * Default values for all options
 *
 * Defined here so that they are instantly available until the actual value can be loaded from
 * storage.
 */
const OPTIONS_DEFAULT = {
	"enable":     true,
	"strictness": "lax",
	"mode":       "self",
	"referer":    "",
	
	// user-interface
	"ui-icon-color-mode":   "system",  // ["system", "default", "custom"]
	"ui-icon-color-custom": "#000000", // #RRGGBB
	
	// whitelisting
	"allow":             "",
	"whitelist-default": true,
	"whitelist-sources": [],
	
	"migrated": false
};


/**
 * Track current add-on options, so that their are always available when resolving a request
 */
// Start with the default options
let options = Object.assign({}, OPTIONS_DEFAULT);
let policy  = new Policy(options.allow);

Promise.resolve().then(() => {
	// Load all currently set options from storage
	return browser.storage.local.get();
}).then((result) => {
	// Update the default options with the real ones loaded from storage
	Object.assign(options, result);
	options["whitelist-sources"] = options["whitelist-sources"].slice(0);
	
	//MIGRATE-0.2.6: Disable default whitelist if the previous one whitelist URL was not set to a default value
	if(typeof(options["whitelist"]) !== "undefined") {
		if(options["whitelist"] !== WHITELIST_LEGACY_URL
		&& !options["whitelist"].includes("://meh.schizofreni.co/")) {
			options["whitelist-default"] = false;
			
			if(options["whitelist"].trim().length > 0) {
				options["whitelist-sources"].push(options["whitelist"]);
			}
		}
		
		options["whitelist"] = undefined;
	}
	
	//Migrate-0.2.6: Remove the possibly hundereds of duplicate original whitelist entries added
	//               by a bug with the previous migration utility
	// This will also deduplicate other list entries. Empty entries are also completely eliminated
	// as they aren't even allowed by the current options UI.
	options["whitelist-sources"] = Array.from(new Set(options["whitelist-sources"]));
	options["whitelist-sources"] = options["whitelist-sources"].filter((entry) => {
		return entry.trim().length > 0;
	});
	
	//MIGRATE-0.2.12: Strictness is now an option list, rather then a single boolean
	if(typeof(options["strict"]) !== "undefined") {
		options["strictness"] = options["strict"] ? "lax" : "strict";
		
		options["strict"] = undefined;
	}
	
	// Write back the final option list so that the defaults are properly displayed on the
	// options page as well
	return browser.storage.local.set(options);
}).then(() => {
	// Do initial policy fetch (will cause timer for more updates to be set)
	refreshPolicy().catch(console.exception);
	
	// Keep track of new developments in option land
	browser.storage.onChanged.addListener((changes, areaName) => {
		if(areaName !== "local") {
			return;
		}
		
		// Copy changes to local options state
		for(let name of Object.keys(changes)) {
			options[name] = changes[name].newValue;
		}
		
		// Apply changes to option keys
		for(let name of Object.keys(changes)) {
			switch(name) {
				case "allow":
				case "whitelist-default":
				case "whitelist-sources":
					refreshPolicy().catch(console.exception);
					break;
				
				case "strictness":
				case "mode":
				case "referer":
					registerContentScript().catch(console.error);
					break;
				
				case "enable":
					applyRefererConfiguration();
					break;
			}
		}
	});
}).catch(console.exception);


/**
 * Shuffle the contents of an array
 *
 * Source: https://stackoverflow.com/a/12646864/277882
 */
function shuffleArray(array) {
    for(let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;  // For easier stacking
}


/**
 * Return all browser URLs worth trying when attempting to resolve the given
 * URL value
 */
function expandWhitelistURL(whitelistURL)
{
	try {
		// Convert IPFS compatiblity schemes to dweb:-format
		let parsed = new URL(whitelistURL);
		let protocol = parsed.protocol.substring(0, parsed.protocol.length-1);
		if(["ipfs", "ipns", "ipld"].includes(protocol)) {
			whitelistURL = `dweb:/${protocol}/${parsed.host}${parsed.pathname}${parsed.search}${parsed.hash}`;
		}
		
		if(whitelistURL.startsWith("dweb:")) {
			let dwebLink = whitelistURL.substr(5);
			if(!dwebLink.startsWith("/")) {
				swebLink = "/" + dwebLink;
			}
			
			let gatewayPaths = DWEB_GATEWAYS.map(prefix => (prefix + dwebLink));
			shuffleArray(gatewayPaths);
			
			// Add special localhost gateway at position 0 with retries
			let dwebPath = (new URL("http://localhost" + dwebLink)).pathname;
			gatewayPaths.splice(0, 0,
				"http://localhost:5001/api/v0/cat?arg=" + encodeURIComponent(dwebPath)
			);
			gatewayPaths.splice(1, 0,
				"http://localhost:5001/api/v0/cat?arg=" + encodeURIComponent(dwebPath)
			);
			gatewayPaths.splice(2, 0,
				"http://localhost:5001/api/v0/cat?arg=" + encodeURIComponent(dwebPath)
			);
			
			return gatewayPaths;
		}
		
		return [whitelistURL];
	} catch(e) {
		// Will trigger message in caller
		return [];
	}
}


/**
 * Download an updated version of the online whitelist
 */
let policyUpdateHandle = 0;
async function refreshPolicy() {
	// Stop any previous policy update timer
	if(policyUpdateHandle > 0) {
		clearTimeout(policyUpdateHandle);
		policyUpdateHandle = -1;
	} else if(policyUpdateHandle < 0) {
		// Another policy update is already in progress
		return;
	}
	
	// Build list of whitelist URLs to fetch
	let whitelistURLs = [];
	if(options["whitelist-default"]) {
		whitelistURLs.push(WHITELIST_DEFAULT_URL);
	}
	whitelistURLs.push.apply(whitelistURLs, options["whitelist-sources"]);
	
	// Asynchronously try to fetch and parse all whitelist sources
	let responses = [];
	for(let whitelistURL of whitelistURLs) {
		responses.push((async (fetchURLs) => {
			let error = null;
			for(let fetchURL of fetchURLs) {
				try {
					let response = await fetch(fetchURL);
					if(response.status < 200 || response.status >= 300) {
						throw response;
					}
					
					return new Policy(await response.text());
				} catch(e) {
					if(error === null) {
						error = e;
					}
				}
			}
			
			// Log message: Failed to retrieve whitelist source: $URL$
			console.error(browser.i18n.getMessage("log_whitelist_update_failed"), whitelistURL);
			console.exception(error);
			return null;
		})(expandWhitelistURL(whitelistURL)));
	}
	
	try {
		let policyParts = await Promise.all(responses)
		let newPolicy = new Policy(options["allow"]);
		for(let policyPart of policyParts) {
			if(policyPart) {
				newPolicy.extend(policyPart);
			}
		}
		policy = newPolicy;
			
		// Log message: Final whitelist has $COUNT$ items: $JSLIST$
		console.info(browser.i18n.getMessage("log_whitelist_update_finished"),
		             policy.list.length, policy.list);
		
		// Refresh registered content script
		if(registeredContentScript !== null) {
			await registerContentScript().catch(console.error);
		}
	} finally {
		// Schedule another policy update
		policyUpdateHandle = setTimeout(() => {
			refreshPolicy().catch(console.exception);
		}, 86400000);
	}
}



/***************/
/* HTTP Header */
/***************/

/**
 * Callback function that will process an about-to-be-sent blocking request and modify
 * its "Referer"-header based on to current options
 */
function requestListener(request) {
	// Find current referer header in request
	let referer = null;
	for(let header of request.requestHeaders) {
		if(header.name.toLowerCase() === "referer" && header.value) {
			referer = header;
			break;
		}
	}
	
	if(referer === null) {
		return;
	}
	
	let updatedReferer = determineUpdatedReferrer(referer.value, request.url, policy, options);
	
	if(updatedReferer === null) {
		return;
	}
	
	// Log message: Rejecting HTTP Referer “$SOURCE$” for “$TARGET$”
	console.debug(browser.i18n.getMessage("log_blocked_http"), referer.value, request.url);
	
	referer.value = updatedReferer;
	return {requestHeaders: request.requestHeaders};
}



let registeredContentScript = null;
async function doRegisterContentScript() {
	// Fortunately, it does not appear like we have to fight with IFrame
	// properties exposing everything here:
	//
	//  1. Either a subframe is from the same domain, then their referrer will
	//     be the current page's URI
	//  2. Or the subframe is from a different domain, but then its contents
	//     will be CORS protected AND will only also be the current page's URI
	//     if it were accessible
	//  3. When trying to scrape the parent frame's URI from the subframe, then
	//     its value won't be good because our content script will always run
	//     before any page script
	let registeredScript = await browser.contentScripts.register({
		allFrames: true,
		matchAboutBlank: true,
		matches: ["*://*/*"],
		
		runAt: "document_start",
		js: [
			{ file: "deps/public-suffix-list/dist/psl.js" },
			
			{ file: "background/policy.js" },
			
			{ code: `
				"use strict";
				
				let policy = Policy.fromJSON(${JSON.stringify(policy.toJSON())});
				
				let options = {
					"enable":     ${JSON.stringify(options["enable"])},
					"strictness": ${JSON.stringify(options["strictness"])},
					"mode":       ${JSON.stringify(options["mode"])},
					"referer":    ${JSON.stringify(options["referer"])},
				};
				let updatedReferrer = determineUpdatedReferrer(
					document.referrer, window.location.href, policy, options
				);
				
				if(updatedReferrer !== null) {
					// Replace 'document.referrer' with a lookalike that returns
					// our modded value
					let getterFunc = exportFunction(() => {
						return updatedReferrer;
					}, document);
					Object.defineProperty(document.wrappedJSObject, "referrer", {
						get: getterFunc,
						configurable: true,
						enumerable: true
					});
					
					// Prevent discovery of the above override using 'Reflect.getOwnPropertyDescriptor'
					let _getOwnPropertyDescriptor = window.wrappedJSObject.Reflect.getOwnPropertyDescriptor;
					let getOwnPropertyDescriptor = exportFunction(function getOwnPropertyDescriptor(target, name) {
						if(target !== document || name !== "referrer") {
							return _getOwnPropertyDescriptor(target, name);
						} else {
							return undefined;
						}
					}, window);
					window.wrappedJSObject.Object.getOwnPropertyDescriptor  = getOwnPropertyDescriptor;
					window.wrappedJSObject.Reflect.getOwnPropertyDescriptor = getOwnPropertyDescriptor;
				}
			` }
		]
	});
	
	if(registeredContentScript !== null) {
		registeredContentScript.unregister();
	}
	registeredContentScript = registeredScript;
}


/*****************
 * Orchestration *
 *****************/


let browserVersion = -1;
browser.runtime.getBrowserInfo().then((info) => {
	browserVersion = parseInt(info.version.split(".")[0]);
});


async function registerContentScript() {
	// Wait for browser version availability
	while(browserVersion < 0) {
		await new Promise((resolve, reject) => {
			setTimeout(resolve, 0);
		});
	}
	
	//COMPAT: Firefox 69+ (https://bugzilla.mozilla.org/show_bug.cgi?id=1601496)
	if(browserVersion >= 69) {
		await doRegisterContentScript();
	}
}


/**
 * Start or stop the HTTP header and JavaScript modifications
 */
let processingEnabled = false;
function applyRefererConfiguration() {
	if(!processingEnabled && options["enable"]) {
		processingEnabled = true;
		browser.webRequest.onBeforeSendHeaders.addListener(
			requestListener,
			{urls: ["<all_urls>"]},
			["blocking", "requestHeaders"]
		);
		registerContentScript().catch(console.error);
	} else if(processingEnabled && !options["enable"]) {
		browser.webRequest.onBeforeSendHeaders.removeListener(requestListener);
		processingEnabled = false;
		
		if(registeredContentScript !== null) {
			registeredContentScript.unregister();
			registeredContentScript = null;
		}
	}
}

// Enable request processing based on the default configuration until the
// actual configuration has been retrieved from disk
applyRefererConfiguration();
