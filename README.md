Smart Referer
=============
Automatically hide HTTP and JavaScript Referer for cross-domain requests!

To workaround (rare) breakage, a whitelist of related domains is regularily
downloaded and applied in the background. You may view and update the current
version of this whitelist
[here](https://gitlab.com/smart-referer/whitelist/blob/master/whitelist.txt).

Installation
------------
*Smart Referer* releases are available for Firefox on
[Firefox Add-ons](https://addons.mozilla.org/firefox/addon/smart-referer/?src=gitlab).
You do not need an account or the Firefox web browser to download releases from
that site, but note that this extension will only work in browsers compatible with
Firefox 56 or newer (including [Waterfox Classic](https://www.waterfox.net/)).
In particular, there are no plans by maintainers to publish a Chrome-compatible
version of this extension, but we would accept contributions if the contributor
takes care of distribution as we will not push to Chrome's store ourselves.

You may also assemble the extension from source by running:

```shell
git clone --recursive https://gitlab.com/smart-referer/smart-referer.git
cd smart-referer
make xpi
```

The assembled extension data will be placed into `web-ext-artifacts/smart-referer.xpi`
inside the source tree. In order to install this file on Firefox for Windows or
macOS, you will either have to switch to the [Developer Edition](https://www.mozilla.org/firefox/developer/)
or create an account on [Firefox Add-ons](https://addons.mozilla.org/) and upload
the extension for signing there. Also, in all Firefox versions and unless you
choose to go through extension signing, you will have to disable extension
signature checking in the browser by opening `about:config` (just type it into
the address bar) and changing the value of “xpinstall.signatures.required” to
`false`.

Options
-------
* *Strict Mode*: When enabled, *Smart Referer* will treat different subdomains
  as being different websites. Therefore `a.example.com` and `b.example.com`
  will not be able to see each other's referer. In general this often causes
  issues and results in little to no privacy improvement, we therefore highly
  recommended leaving this disabled.

* *Exceptions*: A list of different source and destination hosts that should
  never have their referer changed. For instance a rule with Source `*` and
  Destination `*.example.com` will pass referers of all websites to any resource
  served at `example.com` (including its subdomains).

* *Whitelist Sources*: An list of documents containing additional whitelist
  rules. The [default whitelist](https://ipfs.io/ipns/QmYoQ4Gn9vAcimaXT5xWYAPrBCu3QZyLmEvhLFu9djNZCy/whitelist.txt)
  tries to minimize the impact of this extension on everyday web surfing while
  still providing the maximum referer privacy possible under these
  circumstances. See the [default whitelist inclusion guidelines](https://gitlab.com/smart-referer/whitelist/blob/master/README.md#guidelines-for-inclusion-into-the-main-default-whitelist)
  as [this may not be what you want](https://gitlab.com/smart-referer/smart-referer/issues/50).
  
  Misbehaviour in the face of spoofed referers is also not that common anymore,
  so most users should not experience any issues by disabling this feature
  entirely.
  
* *Rewrite Mode*: Can be used to change what is sent to the server instead of
  the original referer header. The default (*Send the URL you're going to as
  referer*) is known to cause the least issues on most sites and is therefore
  recommended.

A website is not working, what should I do?
-------------------------------------------
If a website is not working properly the first thing you can try is making sure
strict mode is disabled.

If the issue isn't solved, you can try adding an exeception for the domain by
adding the source `*.<domain.name>` and the destination `*`. Allowing
`www.example.com` to access everything with the orignal referer you would
therefore add `*.example.com` as the source and `*` as the destination.

If you want to help finding a reasonable whitelist entry that solves the issue
for everyone, [please see this Wiki entry](https://gitlab.com/smart-referer/whitelist/wikis/Gathering-Data-for-Whitelist-Entries)
on how to gather additional information and finding reasonable/minimal rules.

**Please** also [open an issue](https://gitlab.com/smart-referer/whitelist/issues/new)
on the whitelist issue tracker about it (even if you cannot find a solution)
and we'll try to add the proper patterns to the autoupdated whitelist.

Want to help out?
-----------------
Please that a look at the many features proposed in our bug tracker tagged as
[„Help wanted“](https://gitlab.com/smart-referer/smart-referer/issues?state=opened&label_name[]=Help%20Wanted).
Most of these would be nice to have in *Smart Referer* but are unlikely to be
added by the core team.

Also don't be shy to ask questions in the bug tracker!