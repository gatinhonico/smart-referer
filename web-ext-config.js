module.exports = {
	ignoreFiles: [
		// Hidden files are ignored by default
		
		/*****************************
		 * Files also ignored by GIT *
		 *****************************/
		// Ignore temporary files used by some editors
		"**/*.swp",
		"**/*~",
		
		// Project storage of some editors
		".idea/",
		".project/",
		".settings/",
		
		// Stuff created by some desktop environments
		"**/.directory",
		"**/.DS_Store",
		"**/Desktop.ini",
		"**/Thumbs.db",
		
		// Stuff that never was meant to be public
		"+junk/",
		
		// Modules used during development (dependencies use GIT submodules instead)
		"yarn.lock",
		"**/node_modules/",
		
		// API key and secret
		"web-ext-api-secret.txt",
		
		/***************************************
		 * Other files not included in release *
		 ***************************************/
		
		// Ignore documentation resources
		"docs/",
		
		// Ignore screenshots
		"screenshots/",
		
		// Ignore build system files
		"scripts/",
		"web-ext-config.js",
		
		// Ignore TypeScript and related development files
		"**/*.ts",
		"jsconfig.json",
		"package.json",
		
		// Only include the pre-assembled, non-minimized distribution file of the PSL
		"deps/public-suffix-list/*.*",
		"deps/public-suffix-list/data/",
		"deps/public-suffix-list/examples/",
		"deps/public-suffix-list/scripts/",
		"deps/public-suffix-list/test/",
		"deps/public-suffix-list/dist/psl.min.js",
		
		// Only include wext-options main files
		"deps/wext-options/example/",
		"deps/wext-options/screenshots/",
	]
};
